FROM debian:bullseye-slim as builder

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        ca-certificates curl gnupg


ARG GIT_REF
ARG BTCARCH="x86_64-linux-gnu"
ENV CORE_URL="https://bitcoincore.org/bin/bitcoin-core-${GIT_REF}"
ENV TAR="bitcoin-${GIT_REF}-${BTCARCH}.tar.gz" CHECKSUM="SHA256SUMS"
ENV GPG_OPT="--batch --no-tty" GPG_KEYS="keys.txt"
ENV INST_DIR="/opt/bitcoin"

COPY ${GPG_KEYS} .

RUN while read fingerprint keyserver; do \
        gpg --keyserver ${keyserver} --recv-keys ${fingerprint}; \
        done < ${GPG_KEYS} \
    && curl -SLO ${CORE_URL}/${CHECKSUM} \
    && curl -SLO ${CORE_URL}/${CHECKSUM}.asc \
    && curl -SLO ${CORE_URL}/${TAR} \
    && grep " ${TAR}\$" ${CHECKSUM} | sha256sum -c - \
    && GOOD_SIGS=$(gpg ${GPG_OPT} --verify ${CHECKSUM}.asc ${CHECKSUM} 2>&1 \
        |grep 'Good signature' |wc -l) && [ "$GOOD_SIGS" -gt 3 ] || exit 1 \
    && mkdir ${INST_DIR} \
    && tar -xzf ${TAR} -C ${INST_DIR} --strip-components 1 \
    && rm ${TAR} ${CHECKSUM} ${CHECKSUM}.asc

FROM debian:bullseye-slim

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        gosu \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV APP_DIR="/srv/app" USER="blits" INST_DIR="/opt/bitcoin"
RUN adduser --home ${APP_DIR} --shell /bin/bash --disabled-login \
        --gecos "${USER} user" ${USER} \
    && mkdir ${APP_DIR}/.bitcoin \
    && chown ${USER}:${USER} ${APP_DIR}/.bitcoin

COPY --from=builder --chown=${USER}:1000 ${INST_DIR} ${INST_DIR}

COPY start-bitcoind.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/start-bitcoind.sh

ENV PATH="${INST_DIR}/bin:${PATH}"
WORKDIR "${APP_DIR}"
EXPOSE 8332 8333 18332 18333 18443 18444 28332 28333
VOLUME ["${APP_DIR}/.bitcoin"]
ENTRYPOINT ["/usr/local/bin/start-bitcoind.sh"]
