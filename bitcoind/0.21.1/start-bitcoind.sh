#!/bin/bash

_die() {
    echo "$@" 1>&2;
    exit 2
}

# get params
NETWORK=${NETWORK:-"regtest"}
RPCAUTH=${RPCAUTH:-'user:bf93073baa8c70d4ea44ce57dec50395$76c8c2b13f408576681eb80b9a98e92f671986e94501ec24f13e966a1dde4623'}
ADDR_TYPE=${ADDR_TYPE:-"bech32"}

# check params
[[ ! "${NETWORK}" =~ ^(mainnet|testnet|regtest)$ ]] && \
    _die "incorrect network; available networks: mainnet, testnet, regtest"

# set ownership
echo "Setting ownership to files..."
[ -n "${MYUID}" ] && usermod -u "${MYUID}" "${USER}"
[ -n "${MYGID}" ] && groupmod -g "${MYGID}" "${USER}"
chown -R --silent "${USER}:${USER}" "${APP_DIR}"

# construct command
cmd=$(echo \
    "bitcoind -server -printtoconsole -txindex -onlynet=ipv4 -prune=0" \
    "-rpcbind=0.0.0.0 -rpcallowip=172.16.0.0/12" \
    "-rpcallowip=192.168.0.0/16 -rpcallowip=10.0.0.0/8" \
    "-zmqpubrawtx=tcp://0.0.0.0:28333 -zmqpubrawblock=tcp://0.0.0.0:28332" \
    "-rpcauth=${RPCAUTH} -addresstype=${ADDR_TYPE}" \
    "$@"
)
[[ "${NETWORK}" =~ ^(testnet|regtest)$ ]] && \
    cmd="${cmd} -${NETWORK}"

# start service
echo "Starting bitcoind: ${cmd}"
exec gosu "${USER}" ${cmd}
