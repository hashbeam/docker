FROM debian:bullseye-slim as builder

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        autoconf automake build-essential ca-certificates file gettext git \
        libgmp-dev libsqlite3-dev libtool net-tools python3 \
        python3-pip wget zlib1g-dev

ARG BITCOIN_VERSION=0.20.1
ARG BITCOIN_TARBALL=bitcoin-${BITCOIN_VERSION}-x86_64-linux-gnu.tar.gz
ARG BITCOIN_URL=https://bitcoincore.org/bin/bitcoin-core-${BITCOIN_VERSION}/${BITCOIN_TARBALL}
ARG BITCOIN_ASC_URL=https://bitcoincore.org/bin/bitcoin-core-${BITCOIN_VERSION}/SHA256SUMS.asc
RUN mkdir /opt/bitcoin && cd /opt/bitcoin \
    && wget -qO ${BITCOIN_TARBALL} "${BITCOIN_URL}" \
    && wget -qO bitcoin.asc "${BITCOIN_ASC_URL}" \
    && grep ${BITCOIN_TARBALL} bitcoin.asc | tee SHA256SUMS.asc \
    && sha256sum -c SHA256SUMS.asc \
    && BD=bitcoin-${BITCOIN_VERSION}/bin \
    && tar -xzvf ${BITCOIN_TARBALL} ${BD}/bitcoin-cli --strip-components=1 \
    && rm ${BITCOIN_TARBALL}

ARG GIT_REF
ARG DEVELOPER="0"
ENV GIT_URL="https://github.com/ElementsProject/lightning.git"
ENV PYTHON_VERSION=3
RUN git clone --recursive -b ${GIT_REF} --depth 1 ${GIT_URL}
RUN python3 -m pip install mako==1.1.5 mrkd==0.1.6 mistune==0.8.4
RUN cd lightning \
    && mkdir /tmp/lightning_install \
    && ./configure --prefix=/tmp/lightning_install --enable-static \
    && make -j3 DEVELOPER=${DEVELOPER} \
    && make install


FROM debian:bullseye-slim

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        gosu inotify-tools libgmp10 socat sqlite3 tini zlib1g \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV APP_DIR="/srv/app" USER="blits"
RUN adduser --home ${APP_DIR} --shell /bin/bash --disabled-login \
        --gecos "${USER} user" ${USER} \
    && mkdir -p ${APP_DIR}/.lightning \
    && chown ${USER}:${USER} ${APP_DIR}/.lightning

COPY --from=builder --chown=${USER}:1000 /opt/bitcoin/bin/bitcoin-cli /usr/local/bin/
COPY --from=builder --chown=${USER}:1000 /tmp/lightning_install/ /usr/local/

COPY start-cln.sh /usr/local/bin
RUN chmod +x /usr/local/bin/start-cln.sh

WORKDIR "${APP_DIR}"
EXPOSE 9735 9835
VOLUME ["${APP_DIR}/.lightning"]
ENTRYPOINT ["/usr/bin/tini", "-g", "--", "/usr/local/bin/start-cln.sh"]
