#!/bin/bash

_die() {
    echo "$@" 1>&2;
    exit 2
}

# get params
BTCHOST=${BTCHOST:-"bitcoind"}
BTCUSER=${BTCUSER:-"user"}
BTCPASS=${BTCPASS:-"default_password"}
BITCOIN_CLI=${BITCOIN_CLI:-"/usr/local/bin/bitcoin-cli"}
LOG_LEVEL=${LOG_LEVEL:-"info"}
DATA_DIR=${DATA_DIR:-"${APP_DIR}/.lightning"}
NETWORK=${NETWORK:-"regtest"}
ANNOUNCE_ADDR=${ANNOUNCE_ADDR:-""}
BIND_ADDR=${BIND_ADDR:-"0.0.0.0:9735"}
ALIAS=${ALIAS:-"default_alias"}
RGB=${RGB:-"DCDCDC"}
EXPOSE_TCP=${EXPOSE_TCP:-"true"}
LIGHTNINGD_RPC_PORT=${LIGHTNINGD_RPC_PORT:-"9835"}

# check params
[[ ! "${NETWORK}" =~ ^(mainnet|testnet|regtest)$ ]] && \
    _die "incorrect network; available networks: mainnet, testnet, regtest"
[[ ! "${LOG_LEVEL}" =~ ^(io|debug|info|unusual|broken)$ ]] && \
    _die "incorrect log level; available levels: io, debug, info, unusual, broken"

# set ownership
echo "Setting file ownership..."
[ -n "${MYUID}" ] && usermod -u "${MYUID}" "${USER}"
[ -n "${MYGID}" ] && groupmod -g "${MYGID}" "${USER}"
find "${APP_DIR}" \( -not -uid $(id -u ${USER}) -or -not -gid $(id -g ${USER}) \) \
    -exec chown --silent "${USER}:${USER}" "{}" +

# construct command
[ "${NETWORK}" == "mainnet" ] && NETWORK="bitcoin"
LIGHTNINGD_DATA=${DATA_DIR}/${NETWORK}
cmd=$(echo \
    "lightningd" \
    "--log-level=${LOG_LEVEL}" \
    "--lightning-dir=${DATA_DIR}" \
    "--network=${NETWORK}" \
    "--bitcoin-rpcconnect=${BTCHOST}" \
    "--bitcoin-rpcuser=${BTCUSER}" \
    "--bitcoin-rpcpassword=${BTCPASS}" \
    "--bitcoin-cli=${BITCOIN_CLI}" \
    "--bind-addr=${BIND_ADDR}" \
    "--alias=${ALIAS}" \
    "--rgb=${RGB}"
)
[ -n "${ANNOUNCE_ADDR}" ] && cmd="${cmd} --announce-addr=${ANNOUNCE_ADDR}"
cmd="${cmd} $@"

# start service
echo "Starting c-lightning: ${cmd}"
if [ "${EXPOSE_TCP}" == "true" ]; then
    set -m
    gosu "${USER}" ${cmd} &
    while read -r i; do
        if [ "${i}" = "lightning-rpc" ]; then break; fi;
    done \
        < <(inotifywait -e create,open --format '%f' --quiet "${LIGHTNINGD_DATA}" --monitor)
    cmd1="TCP4-listen:${LIGHTNINGD_RPC_PORT},fork,reuseaddr"
    cmd2="UNIX-CONNECT:${LIGHTNINGD_DATA}/lightning-rpc"
    socat "${cmd1}" "${cmd2}" &
    echo "c-lightning started, RPC available on port ${LIGHTNINGD_RPC_PORT}"
    fg %- > /dev/null
else
    exec gosu "${USER}" ${cmd}
fi
