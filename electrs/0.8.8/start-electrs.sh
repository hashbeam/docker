#!/bin/bash

_die() {
    echo "$@" 1>&2;
    exit 2
}

# get params
BTCHOST=${BTCHOST:-"bitcoind"}
BTCPORT=${BTCPORT:-"18443"}
BTCUSER=${BTCUSER:-"user"}
BTCPASS=${BTCPASS:-"default_password"}
PORT=${PORT:-"50001"}
NETWORK=${NETWORK:-"regtest"}
LOG_LEVEL=${LOG_LEVEL:-"info"}

# check params
[[ ! "${NETWORK}" =~ ^(mainnet|testnet|regtest)$ ]] && \
    _die "incorrect network; available networks: mainnet, testnet, regtest"
[[ ! "${LOG_LEVEL}" =~ ^(trace|debug|info|warning)$ ]] && \
    _die "incorrect log level; available levels: trace, debug, info, warning"

# set ownership
echo "Setting ownership to files..."
[ -n "${MYUID}" ] && usermod -u "${MYUID}" "${USER}"
[ -n "${MYGID}" ] && groupmod -g "${MYGID}" "${USER}"
chown -R --silent "${USER}:${USER}" "${APP_DIR}"

# create config file
[ "${NETWORK}" == "mainnet" ] && NETWORK="bitcoin"
declare -A LOG_LEVELS
LOG_LEVELS=( ["warning"]="1" ["info"]="2" ["debug"]="3" ["trace"]="4")
VERBOSITY=${LOG_LEVELS[${LOG_LEVEL}]}
cat <<EOF > /etc/electrs/config.toml
daemon_rpc_addr = "${BTCHOST}:${BTCPORT}"
cookie = "${BTCUSER}:${BTCPASS}"
network = "${NETWORK}"
electrum_rpc_addr = "0.0.0.0:${PORT}"
index_batch_size = 10
jsonrpc_import = true
verbose = ${VERBOSITY}
EOF

# construct command
cmd="cargo run --release -- --db-dir ${APP_DIR}/db $@"

# start service
cd electrs
echo "Starting electrs: ${cmd}"
exec gosu "${USER}" ${cmd}
