FROM rust:1.54.0-slim-buster as builder

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        clang=1:7.* cmake=3.* git libsnappy-dev=1.*

WORKDIR /tmp

ARG GIT_REF
ENV GIT_URL="https://github.com/romanz/electrs.git"
RUN git clone -b ${GIT_REF} --depth 1 ${GIT_URL} \
    && cd electrs \
    && cargo install --locked --path .

FROM debian:buster-slim

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        clang=1:7.* gosu \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV APP_DIR="/srv/app" USER="blits"
RUN adduser --home ${APP_DIR} --shell /bin/bash --disabled-login \
    --gecos "${USER} user" ${USER}

WORKDIR "${APP_DIR}"

COPY --from=builder --chown=${USER}:${USER} \
    /tmp/electrs/target/release/electrs .

RUN mkdir -p /etc/electrs ${APP_DIR}/db
COPY start-electrs.sh /usr/local/bin
RUN chmod +x /usr/local/bin/start-electrs.sh

EXPOSE 50001 4224
STOPSIGNAL SIGINT
VOLUME ["${APP_DIR}/db"]
ENTRYPOINT ["/usr/local/bin/start-electrs.sh"]
