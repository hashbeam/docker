#!/usr/bin/env bash

# make sure required directories exist
mkdir -p ${B_DATA}/certs ${B_DATA}/db ${B_DATA}/logs ${B_DATA}/macaroons

# set ownership
echo "Setting file ownership..." && \
[ -n "${MYUID}" ] && usermod -u "${MYUID}" "${USER}"
[ -n "${MYGID}" ] && groupmod -g "${MYGID}" "${USER}"
find "${B_DATA}" \( -not -uid $(id -u ${USER}) -or -not -gid $(id -g ${USER}) \) \
    -exec chown --silent "${USER}:${USER}" "{}" +

# start service
export PATH="${PATH}:${APP_DIR}/.local/bin"
case $1 in
    # no user cmd, start boltlight
    "")
        echo "Starting boltlight..."
        exec gosu "${USER}" boltlight
        ;;
    # start user cmd
    *)
        echo "Starting custom command \"$@\"..."
        exec gosu "${USER}" "$@"
        ;;
esac
