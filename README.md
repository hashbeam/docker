# Docker images for Bitcoin and the Lightning Network

This project builds docker images for the following services:
* `bitcoind`
* `cln`
* `eclair`
* `electrs`
* `electrum`
* `lnd`
* `boltlight`


## Usage

The `unix_helper` script provides an entry point to all the functionalities.

To get the help message, run it without any arguments or with `help`:
```
$ ./unix_helper help
```

To get a list of available services, use the `services` argument:
```
$ ./unix_helper services
```

To get a list of available versions for a given service, use the `versions`
argument, followed by the service name. An example using `bitcoind`:
```
$ ./unix_helper versions bitcoind
```

To build the docker image for a service, use the `build` argument, followed by
the service name and the desired version. An example using `bitcoind`:
```
$ ./unix_helper build bitcoind 22.0
```

To build docker images for all available services, each at their latest
version, use the `build-all` argument:
```
$ ./unix_helper build-all
```
