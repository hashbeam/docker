#!/usr/bin/env bash

_die() {
    echo "$@" 1>&2;
    exit 2
}

# get params
BTCHOST=${BTCHOST:-"bitcoind"}
BTCUSER=${BTCUSER:-"user"}
BTCPASS=${BTCPASS:-"default_password"}
BTCPORT=${BTCPORT:-"18443"}
ZMQ_BLOCK_PORT=${ZMQ_BLOCK_PORT:-"28332"}
ZMQ_TX_PORT=${ZMQ_TX_PORT:-"28333"}
NETWORK=${NETWORK:-"regtest"}
DATA_DIR=${DATA_DIR:-"$APP_DIR/.eclair"}
ANNOUNCE_ADDR=${ANNOUNCE_ADDR:-""}
BIND_ADDR=${BIND_ADDR:-"0.0.0.0:9735"}
API_ADDR=${API_ADDR:-"0.0.0.0:8080"}
API_PWD=${API_PWD:-"default_password"}
ALIAS=${ALIAS:-"default_alias"}
RGB=${RGB:-"DCDCDC"}
BITCOIND_WLT=${BITCOIND_WLT:-""}
JAVA_OPTS=${JAVA_OPTS:-"-Xmx512m"}

# check params
[[ ! "${NETWORK}" =~ ^(mainnet|testnet|regtest)$ ]] && \
    _die "incorrect network; available networks: mainnet, testnet, regtest"

# set ownership
echo "Setting ownership to files..."
[ -n "${MYUID}" ] && usermod -u "${MYUID}" "${USER}"
[ -n "${MYGID}" ] && groupmod -g "${MYGID}" "${USER}"
chown -R --silent "${USER}:${USER}" "${APP_DIR}"

# construct command
sed -i "s/^PASSWORD=.*/PASSWORD=\'$API_PWD\'/" \
    /usr/local/bin/eclair-cli
BIND_IP=$(echo ${BIND_ADDR} | cut -d: -f1)
BIND_PORT=$(echo ${BIND_ADDR} | cut -d: -f2)
API_IP=$(echo ${API_ADDR} | cut -d: -f1)
API_PORT=$(echo ${API_ADDR} | cut -d: -f2)
cmd=$(echo \
    "bash eclair-node/bin/eclair-node.sh" \
    "-Declair.headless" \
    "-Declair.printToConsole" \
    "-Declair.datadir=${DATA_DIR}" \
    "-Declair.chain=${NETWORK}" \
    "-Declair.bitcoind.host=${BTCHOST}" \
    "-Declair.bitcoind.rpcport=${BTCPORT}" \
    "-Declair.bitcoind.rpcuser=${BTCUSER}" \
    "-Declair.bitcoind.rpcpassword=${BTCPASS}" \
    "-Declair.bitcoind.zmqblock=tcp://${BTCHOST}:${ZMQ_BLOCK_PORT}" \
    "-Declair.bitcoind.zmqtx=tcp://${BTCHOST}:${ZMQ_TX_PORT}" \
    "-Declair.server.binding-ip=${BIND_IP}" \
    "-Declair.server.port=${BIND_PORT}" \
    "-Declair.api.enabled=true" \
    "-Declair.api.binding-ip=${API_IP}" \
    "-Declair.api.port=${API_PORT}" \
    "-Declair.api.password=${API_PWD}" \
    "-Declair.node-alias=${ALIAS}" \
    "-Declair.node-color=${RGB}"
)
if [ ! -z "${ANNOUNCE_ADDR}" ]; then
    ANNOUNCE_IP=$(echo ${ANNOUNCE_ADDR} | cut -d: -f1)
    ANNOUNCE_PORT=$(echo ${ANNOUNCE_ADDR} | cut -d: -f2)
    [ "${BIND_PORT}" != "${ANNOUNCE_PORT}" ] && \
        echo "bind and anounce addresses must be on the same port" && exit 2
    cmd="${cmd} -Declair.server.public-ips.0=${ANNOUNCE_IP}"
fi
if [ ! -z "${BITCOIND_WLT}" ]; then
    cmd="${cmd} -Declair.bitcoind.wallet=${BITCOIND_WLT}"
fi
cmd="${cmd} $@"
export JAVA_OPTS="${JAVA_OPTS}"

# start service
echo "Starting eclair: ${cmd}"
exec gosu "${USER}" ${cmd}
