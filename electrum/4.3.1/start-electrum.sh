#!/bin/bash

_die() {
    echo "$@" 1>&2;
    exit 2
}

# get params
SRV_HOST=${SRV_HOST:-""}
SRV_PORT=${SRV_PORT:-"50001"}
SRV_PROTOCOL=${SRV_PROTOCOL:-"t"}
DAEMON=${DAEMON:-"1"}
NETWORK=${NETWORK:-"regtest"}

# check params
[[ ! "${NETWORK}" =~ ^(mainnet|testnet|regtest)$ ]] && \
    _die "incorrect network; available networks: mainnet, testnet, regtest"
[ -z "${WALLET_NAME}" ] && _die "env variable WALLET_NAME must be set"
echo "${WALLET_NAME}" | grep -q ' ' && \
    _die "environment variable WALLET_NAME must not contain spaces"
[ -n "${SRV_HOST}" -a "${SRV_PROTOCOL}" != "t" -a "${SRV_PROTOCOL}" != "s" ] && \
    echo "invalid server protocol. options: t (insecure) or s (secure)"

# set ownership
echo "Setting file ownership..."
[ -n "${MYUID}" ] && usermod -u "${MYUID}" "${USER}"
[ -n "${MYGID}" ] && groupmod -g "${MYGID}" "${USER}"
for DIR in ${APP_DATA} ${WALLET_DIR}; do
    find "${DIR}" \( -not -uid $(id -u ${USER}) -or -not -gid $(id -g ${USER}) \) \
        -exec chown --silent "${USER}:${USER}" "{}" +
done

# construct command
params="-D ${WALLET_DIR}/${WALLET_NAME}"
[[ "${NETWORK}" =~ ^(testnet|regtest)$ ]] && params="${params} --${NETWORK}"
cmd="${APP_DIR}/.local/bin/electrum daemon ${params}"
if [ -n "${SRV_HOST}" ]; then
    SERVER_ADDR=${SRV_HOST}:${SRV_PORT}:${SRV_PROTOCOL}
    cmd="${cmd} --oneserver --server ${SERVER_ADDR}"
fi
cmd="${cmd} $@"

# create alias
echo "setting alias '${WALLET_NAME}' to easily launch electrum"
echo "alias ${WALLET_NAME}=\"electrum ${params}\"" >> ${APP_DIR}/.bashrc

# start service
echo "Starting electrum: ${cmd}"
until [ -d ${WALLET_DIR}/${WALLET_NAME} ]; do
    echo "waiting for wallet ${WALLET_DIR}/${WALLET_NAME} to appear..."
    sleep 1
done
exec gosu "${USER}" ${cmd}
