FROM debian:buster-slim

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        build-essential git gosu libsecp256k1-0 protobuf-compiler \
        python3-cryptography python3-dev python3-pip python3-pyqt5 \
        python3-setuptools python3-six \
    && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV APP_DIR="/srv/app" USER="blits" WALLET_DIR="/srv/wallet"
ENV LANG="C.UTF-8" PYTHONUNBUFFERED=1 PYTHONIOENCODING="UTF-8"

RUN adduser --home ${APP_DIR} --shell /bin/bash --disabled-login \
    --gecos "${USER} user" ${USER}

RUN mkdir ${WALLET_DIR} \
    && chown -R ${USER}:${USER} /srv/*

WORKDIR ${APP_DIR}
USER ${USER}

ARG GIT_REF
ENV GIT_URL="https://github.com/spesmilo/electrum.git"
RUN git clone --recursive ${GIT_URL} \
    && cd electrum \
    && git checkout ${GIT_REF} \
    && python3 -m pip install -U pip \
    && python3 -m pip install --user --no-warn-script-location .[gui,crypto] \
    && protoc --proto_path=electrum \
        --python_out=electrum electrum/paymentrequest.proto

USER root
COPY start-electrum.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/start-electrum.sh

EXPOSE 7777 9735
ENV PATH="${APP_DIR}/.local/bin:${PATH}"
STOPSIGNAL SIGINT
VOLUME ["${WALLET_DIR}"]
ENTRYPOINT ["/usr/local/bin/start-electrum.sh"]
