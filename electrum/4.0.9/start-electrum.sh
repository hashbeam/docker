#!/bin/bash

_die() {
    echo "$@" 1>&2;
    exit 2
}

# get params
SRV_HOST=${SRV_HOST:-""}
SRV_PORT=${SRV_PORT:-"50001"}
SRV_PROTOCOL=${SRV_PROTOCOL:-"t"}
DAEMON=${DAEMON:-"1"}
NETWORK=${NETWORK:-"regtest"}

# check params
[[ ! "${NETWORK}" =~ ^(mainnet|testnet|regtest)$ ]] && \
    _die "incorrect network; available networks: mainnet, testnet, regtest"
[ -z "${WALLET_NAME}" ] && _die "env variable WALLET_NAME must be set"
echo "${WALLET_NAME}" | grep -q ' ' && \
    _die "environment variable WALLET_NAME must not contain spaces"
[ -n "${SRV_HOST}" -a "${SRV_PROTOCOL}" != "t" -a "${SRV_PROTOCOL}" != "s" ] && \
    echo "invalid server protocol. options: t (insecure) or s (secure)"

# set ownership
echo "Setting ownership to files..."
[ -n "${MYUID}" ] && usermod -u "${MYUID}" "${USER}"
[ -n "${MYGID}" ] && groupmod -g "${MYGID}" "${USER}"
chown -R --silent "${USER}:${USER}" "${APP_DIR}"
chown -R --silent "${USER}" "${WALLET_DIR}"

# construct command
params="-D ${WALLET_DIR}/${WALLET_NAME}"
[[ "${NETWORK}" =~ ^(testnet|regtest)$ ]] && params="${params} --${NETWORK}"
mode="daemon"
[ "${DAEMON}" == "0" ] && mode="gui"
cmd="${APP_DIR}/.local/bin/electrum ${mode} ${params}"
if [ -n "${SRV_HOST}" ]; then
    SERVER_ADDR=${SRV_HOST}:${SRV_PORT}:${SRV_PROTOCOL}
    cmd="${cmd} --oneserver --server ${SERVER_ADDR}"
fi
cmd="${cmd} $@"

# create alias
echo "setting alias '${WALLET_NAME}' to easily launch electrum"
echo "alias ${WALLET_NAME}=\"electrum ${params}\"" >> ${APP_DIR}/.bashrc

# start service
echo "Starting electrum: ${cmd}"
exec gosu "${USER}" ${cmd}
