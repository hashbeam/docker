#!/bin/bash

_die() {
    echo "$@" 1>&2;
    exit 2
}

# get params
BACKEND="bitcoind"
CHAIN="bitcoin"
BTCHOST=${BTCHOST:-"bitcoind"}
BTCUSER=${BTCUSER:-"user"}
BTCPASS=${BTCPASS:-"default_password"}
ZMQ_BLOCK_PORT=${ZMQ_BLOCK_PORT:-"28332"}
ZMQ_TX_PORT=${ZMQ_TX_PORT:-"28333"}
LOG_LEVEL=${LOG_LEVEL:-"info"}
DATA_DIR=${DATA_DIR:-"/srv/app/.lnd"}
NETWORK=${NETWORK:-"regtest"}
ANNOUNCE_ADDR=${ANNOUNCE_ADDR:-""}
BIND_ADDR=${BIND_ADDR:-"0.0.0.0:9735"}
API_ADDR=${API_ADDR:-"0.0.0.0:10009"}
TLS_EXTRA_IP=${TLS_EXTRA_IP:-"127.0.0.1"}
TLS_EXTRA_DNS=${TLS_EXTRA_DNS:-"localhost"}
NO_MACAROONS=${NO_MACAROONS:-"0"}
ALIAS=${ALIAS:-"default_alias"}
RGB=${RGB:-"DCDCDC"}

# check params
[[ ! "${NETWORK}" =~ ^(mainnet|testnet|regtest)$ ]] && \
    _die "incorrect network; available networks: mainnet, testnet, regtest"
[[ ! "${LOG_LEVEL}" =~ ^(trace|debug|info|warn|error|critical)$ ]] && \
    _die "incorrect log level; available levels: trace, debug, info, warn, error, critical"

# set ownership
echo "Setting ownership to files..."
[ -n "${MYUID}" ] && usermod -u "${MYUID}" "${USER}"
[ -n "${MYGID}" ] && groupmod -g "${MYGID}" "${USER}"
chown -R --silent "${USER}:${USER}" "${APP_DIR}"

# construct command
cmd=$(echo \
    "lnd" \
    "--lnddir=${DATA_DIR} --alias=${ALIAS} --color=#${RGB}" \
    "--${CHAIN}.active --${CHAIN}.node=${BACKEND}" \
    "--${BACKEND}.rpchost=${BTCHOST} --${BACKEND}.rpcuser=${BTCUSER}" \
    "--${BACKEND}.rpcpass=${BTCPASS} --rpclisten=${API_ADDR}" \
    "--listen=${BIND_ADDR} --debuglevel=${LOG_LEVEL} --tlsautorefresh" \
    "--${BACKEND}.zmqpubrawblock=tcp://${BTCHOST}:${ZMQ_BLOCK_PORT}" \
    "--${BACKEND}.zmqpubrawtx=tcp://${BTCHOST}:${ZMQ_TX_PORT}"
)
[[ "${NETWORK}" =~ ^(testnet|regtest)$ ]] && cmd="${cmd} --${CHAIN}.${NETWORK}"
[ ! -n "${TLS_EXTRA_IP}" ] && cmd="${cmd} --tlsextraip=${TLS_EXTRA_IP}"
[ ! -n "${TLS_EXTRA_DNS}" ] && cmd="${cmd} --tlsextradomain=${TLS_EXTRA_DNS}"
[ "${NO_MACAROONS}" == "1" ] && cmd="${cmd} --no-macaroons"
[ -n "${ANNOUNCE_ADDR}" ] && cmd="${cmd} --externalip=${ANNOUNCE_ADDR}"
cmd="${cmd} $@"

# start service
echo "Starting lnd: ${cmd}"
exec gosu "$USER" ${cmd}
